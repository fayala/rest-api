var gulp = require('gulp');
var gls = require('gulp-live-server');
var mocha = require('gulp-mocha');
var sonar = require('gulp-sonar');
var istanbul = require('gulp-istanbul');

const del = require('del');
const sonar_host_url = process.env.SONAR_HOST_URL || 'http://localhost:9000';

gulp.task('clean', function () {
  del('node_modules/**');
  del('reports/**');
  del('coverage/**');

});

gulp.task('xunit-report', function () {
  return gulp.src(['test/spec.js'], { read: false })
    .pipe(mocha({
      reporter: 'mocha-junit-reporter',
      reporterOptions: {
        mochaFile: './reports/test-reports.xml',
      }
    }));
});

gulp.task('unit-test', function () {
  return gulp.src(['test/spec.js'], { read: false })
    .pipe(mocha({
      reporter: 'mochawesome',
      reporterOptions: {
        reportDir: 'reports',
        reportFilename: 'mochawesome',
        enableCharts: false
      }
    }));
});

gulp.task('test', function () {
  return gulp.src(['test/*.js'])
    .pipe(mocha({
      reporter: 'mochawesome',
      reporterOptions: {
        reportDir: 'reports',
        reportFilename: 'mochawesome',
        enableCharts: false
      }
    }
    ))
    // Creating the reports after tests ran 
    .pipe(istanbul.writeReports())
    // Enforce a coverage of at least 90% 
    .pipe(istanbul.enforceThresholds({ thresholds: { global: 90 } }));
});

gulp.task('sonar', function () {
  var options = {
    sonar: {
      host: {
        url: sonar_host_url
      },

      projectKey: 'FAFA:REST_API:1.0',
      projectName: 'FAFA REST API',
      projectVersion: '1.0.0',
      // comma-delimited string of source directories 
      sources: 'bin, public, routes, views, test',
      language: 'js',
      sourceEncoding: 'UTF-8',
      javascript: {
        lcov: {
          reportPath: 'coverage/lcov.info'
        }
      },
      exec: {
        maxBuffer: 1024 * 1024
      }
    }
  };

  // gulp source doesn't matter, all files are referenced in options object above 
  return gulp.src('thisFileDoesNotExist.js', { read: false })
    .pipe(sonar(options))
    .on('error', console.log);
});

gulp.task('run', function () {
  var server = gls.new('./bin/www');
  server.start();
});


gulp.task('default', function () {
  console.log("Hellow gulp task!");
});